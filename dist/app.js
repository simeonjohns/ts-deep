"use strict";
// const cars: string[] = ['Ford', 'Audi'];
// const cars2: Array<string> = ['Ford', 'Audi'];
// const promise = new Promise<string>(res =>
// 	setTimeout(
// 		() => res('Promise resolved'),
// 		1000,
// 	)
// );
// promise.then(data => console.log(data.toUpperCase()));
//=====================================
function mergeObjects(a, b) {
    return Object.assign({}, a, b);
}
const merged = mergeObjects({ name: 'Simeon' }, { age: 22 });
const merged2 = mergeObjects({ lol: 'LOOLL' }, { kek: 'KEKW' });
const merged3 = mergeObjects({ merged }, { lol: 2234 });
function withCount(value) {
    return {
        value,
        count: `В этом объектк ${value.length} символов`,
    };
}
// console.log(withCount('qweqeqwe'));
// console.log(withCount([1, 23, 434]));
// console.log(withCount({ length: 23 }));
// ==============================
function getObjectValue(obj, key) {
    return obj[key];
}
const person = {
    name: 'Simeon',
    age: 22,
    job: 'Frontend',
};
// console.log(getObjectValue(person, 'name'));
// console.log(getObjectValue(person, 'age'));
// console.log(getObjectValue(person, 'job'));
// ==================================
class Collection {
    // private _items: T[] = []
    constructor(_items = []) {
        this._items = _items;
    }
    add(item) {
        this._items.push(item);
    }
    remove(item) {
        this._items = this._items.filter(i => i !== item);
    }
    get items() {
        return this._items;
    }
}
function createAndValidateCar(model, year) {
    const car = {};
    if (model.length > 3) {
        car.model = model;
    }
    if (year > 2000) {
        car.year = year;
    }
    return car;
}
// ========================
const cars = ['Ford', 'Lol'];
// console.log(cars.shift());
console.log(cars[1]);
const ford = {
    model: 'Ford',
    year: 2020,
};
// ford.model = 'Lol';
//# sourceMappingURL=app.js.map