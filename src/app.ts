// const cars: string[] = ['Ford', 'Audi'];
// const cars2: Array<string> = ['Ford', 'Audi'];

// const promise = new Promise<string>(res =>
// 	setTimeout(
// 		() => res('Promise resolved'),
// 		1000,
// 	)
// );

// promise.then(data => console.log(data.toUpperCase()));


//=====================================

function mergeObjects<T extends object, R extends object>(a: T, b: R): T & R {
	return Object.assign({}, a, b);
}

const merged = mergeObjects({ name: 'Simeon' }, { age: 22 });
const merged2 = mergeObjects({ lol: 'LOOLL' }, { kek: 'KEKW' });
const merged3 = mergeObjects({ merged }, { lol: 2234 });

// console.log({
// 	merged,
// 	merged2,
// 	merged3,
// });


// ==============================

interface ILength {
	length: number,
}

function withCount<T extends ILength>(value: T): { value: T, count: string } {
	return {
		value,
		count: `В этом объектк ${value.length} символов`,
	};
}

// console.log(withCount('qweqeqwe'));
// console.log(withCount([1, 23, 434]));
// console.log(withCount({ length: 23 }));


// ==============================

function getObjectValue<T extends object, R extends keyof T>(obj: T, key: R): T[R] {
	return obj[key];
}

const person = {
	name: 'Simeon',
	age: 22,
	job: 'Frontend',
};

// console.log(getObjectValue(person, 'name'));
// console.log(getObjectValue(person, 'age'));
// console.log(getObjectValue(person, 'job'));


// ==================================

class Collection<T extends number | string | boolean> {
	// private _items: T[] = []

	constructor(private _items: T[] = []) { }

	add(item: T) {
		this._items.push(item);
	}

	remove(item: T) {
		this._items = this._items.filter(i => i !== item);
	}

	get items(): T[] {
		return this._items;
	}
}

// const strings = new Collection<string>(['Hello', ',', 'World']);
// strings.add('!');
// strings.remove(',');
// console.log(strings.items);

// const numbers = new Collection<number>([1, 1, 2, 3, 5, 8]);
// numbers.add(13);
// numbers.remove(1);
// console.log(numbers.items);

// const objects = new Collection<object>([
// 	{ a: 1 },
// 	{ b: 2 },
// ]);
// objects.add({ c: 3 });
// objects.remove({ b: 2 });
// console.log(objects.items);

// ================					

interface Car {
	model: string,
	year: number
}

function createAndValidateCar(model: string, year: number): Car {
	const car: Partial<Car> = {};

	if (model.length > 3) {
		car.model = model;
	}

	if (year > 2000) {
		car.year = year;
	}

	return car as Car;
}


// ========================

const cars: Readonly<Array<string>> = ['Ford', 'Lol'];

// console.log(cars.shift());
console.log(cars[1]);

const ford: Readonly<Car> = {
	model: 'Ford',
	year: 2020,
}

// ford.model = 'Lol';